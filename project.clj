(defproject turtle "0.1.0-SNAPSHOT"
  :description "Turtle Graphics in ClojureScript"
  :url "http://github.com/tiagoantao/turtle"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.9.0-alpha17"]
                 [org.clojure/clojurescript "1.9.908"]
                 [org.clojure/tools.reader "1.0.2"]
                 [org.clojure/core.async  "0.3.443"]
                 [cljs-http "0.1.43"]
                 [cljsjs/paperjs "0.9.24-0"]]
                 

  :plugins [[lein-figwheel "0.5.13"]
            [lein-codox "0.10.3"]
            [lein-cljsbuild "1.1.5" :exclusions [[org.clojure/clojure]]]
            [lein-doo "0.1.7"]]

  ;:clean-targets ^{:protect false} [:target-path "resources/public/js" "out"]

  :repl-options {:timeout 120000}

  :doo {:build "test" :paths {:default [:phantom] }}
  
  :cljsbuild
  {:builds {
            :dev
            {:source-paths ["src"]

             ;; the presence of a :figwheel configuration here
             ;; will cause figwheel to inject the figwheel client
             ;; into your build
             :figwheel {:on-jsload "turtle.paperjs/on-js-reload"
                                        ;:open-urls ["http://localhost:3449/index.html"]
                        }

             :compiler {:main turtle.paperjs
                        :asset-path "js/compiled/out"
                        :output-to "resources/public/js/compiled/turtle.js"
                        :output-dir "resources/public/js/compiled/out"
                        :source-map-timestamp true
                        :preloads [devtools.preload]
                        :pretty-print false
                        :optimizations :none
                        :static-fns true}}

            :test
            {:source-paths ["src" "test"]
             :compiler {:output-to "out/testable.js"
                        :main turtle.runner
                        :process-shim false
                        :optimizations :none
                        }}}}

  :codox {
          :output-path "docs"
          :language :clojurescript
          :source-paths ["src"]
          :metadata {:doc/format :markdown}
          }

  
  :figwheel {:css-dirs ["resources/public/css"] ;; watch and update CSS
             :nrepl-port 7888
             }

  :aliases {"test" ["doo" "phantom" "test" "once"]}

  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.2"]
                                  [figwheel-sidecar "0.5.13"]
                                  [com.cemerick/piggieback "0.2.1"]]
                   ;; need to add dev source path here to get user.clj loaded
                   :source-paths ["src" "dev"]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                   :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                                     :target-path]}})
