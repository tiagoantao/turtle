(ns turtle.paperjs
  "PaperJS backend"
  {:author "Tiago Antao"}
  (:require [cljsjs.paperjs]
            [cljs.js :as cljs]  [cljs.tools.reader :refer [read-string]]; to be moved elsewhere
            [turtle.core :as turtle]
            [turtle.figures :as fig]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]])
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:use     [turtle.rendering :only [play-movements render-state]]))

(enable-console-print!)

(defn draw-state [canvas start end]
  (prn 1234 end)
  (if (and (end :draw) (end :down))
    (let [width (.-width canvas)
          height (.-height canvas)
          x0 (start :x) y0 (- 1000 (start :y))
          x1 (end :x) y1 (- 1000 (end :y))
          p0 (new js/paper.Point (* x0 (/ width 1000)) (* y0 (/ height 1000)))
          p1 (new js/paper.Point (* x1 (/ width 1000)) (* y1 (/ height 1000)))
          path  (new js/paper.Path)]
      (set! (.-strokeColor path) (end :color "black"))
      (.add path p0)
      (.add path p1))))

(defn draw
  "Draws a set of movements on a HTML canvas.
  "
  ([canvas movements start]
   (let [fmovements (flatten movements)
         final-state (play-movements draw-state canvas fmovements start)]
     (.draw (.-view js/paper))
     final-state))

  ([canvas movements] (draw canvas movements turtle/home)))

(defn paper-draw [canvas-name states]
  (set! (.-onload js/window)
        (let [canvas (.getElementById js/document canvas-name)] 
          (.setup js/paper canvas)
          (draw canvas states))))

(defn ^:export multi-tri
  "A test function to draw a few triangles"
  [canvas-name]
  (paper-draw canvas-name
              (list
               (turtle/pos 800 500)
               (turtle/color "blue")
               (repeat 8
                   [(turtle/left 45)
                    (fig/square 100)])
                 (turtle/pos 300 500 15)
                 (turtle/color "red")
                 (turtle/fwd 50)
                 (repeat 10
                         [(turtle/right 30)
                          (turtle/push)
                          (fig/triangle 200)
                          (turtle/pop)])

                 (turtle/pos 100 100) (turtle/color "green")
                 (turtle/scale 0.8 0.8 (fig/circle 50 10))
                 (turtle/pos 100 900) (turtle/color "purple")
                 (fig/star 100 7)
                 (turtle/pos 0 0) (turtle/color "black")
                 (turtle/line-to 1000 1000)

                 (turtle/pos 500 200) (turtle/direction 270) (turtle/fwd 200)
                 (turtle/left 90) (turtle/fwd 200)
                 (turtle/left 90) (turtle/fwd 200)
                 (turtle/left 90) (turtle/fwd 200)
                 (turtle/right 120) (turtle/fwd 200)
                 (turtle/right 120) (turtle/fwd 200)
                 
                 )))


;XXX incorrect URL
(defn loadfn [mp cb]
  (go
    (let [path (str "http://localhost:3449/" (:path mp)
                    (if (nil? (:macros mp)) ".cljs" ".cljc"))
          response (<! (http/get path))]
      (prn 999 mp path (:status response))
      (cb {:lang :clj
           :source (if (= 200 (:status response))
                     (:body response)
                     ""
                       )}))))

(defn on-js-reload []
  (multi-tri "myCanvas") ; XXX canvas name hardcoded
  )

(defn ^:export eval
  [strg]
  (let [state (cljs/empty-state)
        astate (atom @state)]
    (cljs/eval-str astate "(require-macros '[turtle.macros :refer [defturtle]])" "" identity)
    (cljs/eval-str astate "(require '[turtle.core :as t])" "" identity)
    (cljs/eval-str astate "(require '[turtle.figures :as f])" "" identity)

    
    (let [value (:value (cljs/eval-str astate (str "(list " strg ")") "" {:eval cljs/js-eval} identity))]
      (paper-draw "interactive" value)
      )))


;talk about :eval function
;talk about empty-state

(def state (cljs/empty-state))
(def astate (atom @state))


(set! cljs/*load-fn* loadfn)
(set! cljs/*eval-fn* cljs/js-eval)

(cljs/eval-str astate "(require-macros '[turtle.macros :refer [defturtle]])" "" identity)
(cljs/eval-str astate "(require '[turtle.core :as t])" "" identity)
(cljs/eval-str astate "(require '[turtle.figures :as f])" "" identity)


;Notes:
; 1. state/astate is being shared, even if that is not the intention
; 2. Careful with the async code, one needs to wait



