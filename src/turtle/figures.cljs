(ns turtle.figures
  "Library of figures.

Note that no assurances are made on the state of the turtle after
using these functions. Push the state if you want to save it."
  {:author "Tiago Antao"}
  (:require [turtle.core :refer [angle back deg2rad down fwd home left line-to pos right stack-walks up]]))

(enable-console-print!)

(defn triangle
  "Equilateral triangle"
  [size]

  ;#((fwd size) (first ((fwd size) %))))
  
  ;(list (fwd size) (fwd 200)))

  (stack-walks
    [(angle 120)
     (fwd size) (right)
     (fwd size) (right)
     (fwd size) (angle 120)]))



(defn star
  "Star, with n sides"
  [radius sides]
  (stack-walks
   [(angle (/ 360 sides))
    (repeat sides [(fwd radius) (back radius) (right)])]
   ))


(defn square [side-size]
  "A square"
    (stack-walks
     [(repeat 4 [(fwd side-size) (left 90)])]
       ))

(defn circle
  "A 'circle' defined by a number of sides"
  [radius sides]
  #(let [rotation (/ 360 sides)
        {x :x y :y} %
        lines (loop [count 0 curr-states '() state [%] down false]
                (let [angle (* rotation count)
                      rad (deg2rad angle)
                      px (+ x (* radius (Math/cos rad)))
                      py (+ y (* radius (Math/sin rad)))
                      new-state ((line-to px py) (assoc (first state) :down down))
                      down-states (concat curr-states new-state)]
                  (if (= count sides)
                    (flatten down-states)
                    (recur (+ 1 count) down-states new-state true)))
                )]
     (concat lines ((pos (:x %) (:y %)) %))
     ))

;difference between polygon and circle is just the starting point and algo?

((circle 100 2) home)

((circle 50 4) home)

((line-to 10 20) home)

