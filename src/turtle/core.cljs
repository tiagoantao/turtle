(ns turtle.core
  "Turtle graphics core

Implements the core functions for turtle graphics.

These is typical stuff: 'move forward', 'rotate', 'push current position'...

All the code here is independent of the graphics front-end used.

Units are pixels for movement and degrees (not radians) for angles.

"
  {:author "Tiago Antao"}
  (:require [cljs.js :as cljs])
  ;(:require-macros [turtle.macros :refer [scale]])
  )

(enable-console-print!)

(def state (cljs/empty-state))

(defn deg2rad
  "Converts degrees to radians"
  [deg]
  (/ (* Math/PI deg) 180))

(defmulti prn-states (fn [s & args] (map? s)))
(defmethod prn-states true [state & args]
  (if (some #(= :all %) args)
    (prn state)
    (prn {:x (:x state)})))
(defmethod prn-states false [state & args]
  (prn-states (first state))
  (prn)
  (prn-states (rest state))
  )

(def home
  ^{:doc "defines the 'home' position, center of the drawing area, going up"}
  {:x 500 :y 500 :direction 90 :draw true ; draw is "internal"
   :down true :color "#888888"
   :angle 90  ; Default angle for turns
   :stack []})

;draw pertains to an operation, e.g. pos does not draw but fwd does (as
;  long as turtle is down)


(defn pos
  "Sets the current position, optionally also the direction"
  ([x y] #((pos x y (:direction %)) %))
  ([x y d] #(vector (merge % {:x x :y y :direction d :draw false}))))

(defn pop
  "Pops a position state from the stack.
   This will only pop x, y and direction."
  []
  #(let [stack-head (first (% :stack))
         x (stack-head :x)
         y (stack-head :y)
         direction (stack-head :direction)]
     (vector (merge % {:stack (rest (% :stack)) :x x :y y :direction direction}))))

(defn push
  "Pushes a position state into the stack.
   This will only push x, y and direction."
  []
  #(vector (assoc % :stack
          (into [{:x (% :x) :y (% :y) :direction (% :direction)}]
                (% :stack)))))


(defn push-all
  "Pushes everything"
  [what]
  #(assoc % :stack (into [what] (:stack %))))


(defn push-ops
  "Pushes the result of operations"
  [ & ops]
  (fn [state]
    (let [new-stack (loop [state state ops ops stack []]
                      (if (= (count ops) 0) stack
                          (let [op (first ops)
                                remaining (rest ops)
                                new-state (op state)]
                            (recur (first new-state) remaining (into stack new-state)))))]
      [(assoc state :stack (into new-stack (:stack state)))])))

(defn draw-stacked [num-pops & objs]
  "Number of pops from the stack, if <= 0 then all will be popped.

  Only x, y and direction will be popped"
  #(loop [num-pops num-pops
          popped ((pop) %)
          draw '()]
     (let [state (first popped)
           my-draw ((stack-walks objs) state)]
       (prn 4321 (:stack state))
       (if (or
            (= num-pops 0)
            (= (:stack state) []))
         my-draw
         (recur (dec num-pops) state (concat draw my-draw))
       ))))

(defn color
  "Sets the turtle color"
  [color]
  #(vector (assoc % :color color :draw false)))

(defn fwd
  "Advances the turtle"
  [distance]
  #(let [nx (+ (% :x) (* distance (Math/cos (deg2rad (% :direction)))))
         ny (+ (% :y) (* distance (Math/sin (deg2rad (% :direction)))))]
     (vector (merge % {:x nx :y ny :draw true}))))

(defn back
  "Moves the turtle backwards"
  [distance]
  (fwd (- distance)))

(defn line-to [x y]
  #(vector (merge % {:x x :y y :draw true})))
 ; #(merge % {:x x :y y :draw true}))

(defn angle
  "Sets the default angle for right and left.
   Not to be confused with direction."
  [angle]
  #(vector (assoc % :angle angle :draw false)))

(defn direction
  "Sets the direction of the turtle."
  [direction]
  #(vector (assoc % :direction direction :draw false)))

(defn left
  "Rotates the turtle left. If no angle is provided it will used
   the one on the :angle state."
  ([angle] #(vector (assoc % :direction (+ (% :direction) angle))))

  ([]      #(vector (assoc % :direction (+ (% :direction) (% :angle))))))


(defn right
  "Rotates the turtle right. If no angle is provided it will used
  the one on the :angle state."

  ([angle] #(vector (assoc % :direction (- (% :direction) angle))))

  ([]      #(vector (assoc % :direction (- (% :direction) (% :angle))))))


(defn down
  "When the turtle walks, it will write"
  [] #(vector (assoc % :down true :draw false)))

(defn up
  "When the turtle walks, it will NOT write"
  [] #(vector (assoc % :down false :draw false)))

(defn stack-walks [walks]
  #(let [fwalks (flatten walks)]
     (loop [rules fwalks state (list %)]
      (let [[rule & tail] rules]
        (if (nil? tail)
          (concat state (rule (last state)) )
          (recur tail (flatten (concat state (list (rule (last state)))))))))))


(defn scale [xs ys & ops]
  (fn [state]
    (let [start-state   (conj state {:x 0 :y 0})
          scaled-states (loop [xs xs
                               ys ys
                               ops ops
                               generated-states ()
                               state start-state]
                          (if (= (count ops) 0)
                            generated-states
                            (let [op (first ops)
                                  remaining (rest ops)
                                  unscaleds (op state)
                                  scaleds (map #(assoc %
                                                 :x (* xs (% :x))
                                                 :y (* ys (% :y))) unscaleds)
                                  ]
                              (recur xs ys remaining (concat generated-states scaleds) (first unscaleds))
                              )))]
      (map #(assoc % :x (+ (state :x) (% :x)) :y (+ (state :y) (% :y))) scaled-states)
      )))

(comment


  (prn-states {:x 1 :y 1 :direction 30 :bla 1} :all)
  (prn-states {:x 1 :y 1 :direction 30 :bla 1})


  ((push 1) home)

  ((push-ops (fwd 100)) home)
  ((push-ops (fwd 100) (left)) home)

  (assoc home :stack (into [1] (:stack home)))

  ((scale 0.1 0.1 (fwd 100) (left) (fwd 100)) home)

  ((fwd 1) home)
  (into [] '(1 2 3))
                                        ;(prn (macroexpand '(scale 0.1 0.1 [home])))
                                        ;(prn 99 ((scale 0.1 0.1 [(fwd 100) (right) (fwd 200)]) home))


  (def pushy ((push-ops (fwd 100) (left) (fwd 200)) home))


  ((pop) (first pushy))

  ((draw-stacked -1 (fwd 100)) (first pushy))

  ((stack-walks [(fwd 50)]) home)



  (fwd 1)
  ((fwd 1) {:x 0 :y 0 :direction 270})
  ((back 1) {:x 0 :y 0 :direction 90})

  ((fwd 1) home)

  (left 45)

  ((angle 45) home)
  ((left 45) home)
  ((left) home)
  ((right 45) home)
  ((right) home)


  ((pos 100 100) home)
  )
