(ns turtle.macros
  "Turtle state macros
"
  {:author "Tiago Antao"})

(defmacro scale [xs ys what]
  (let [ops (gensym "ops")]
    `(fn [~ops] (map #(assoc % :x (* ~xs (:x %)) :y (* ~ys (:y %))) ~what))))
  ;'(map #(assoc % :x (* ~xs (:x %)) :y (* ~ys (:y %))) ~what))

(defmacro defturtle [name params body]
  (let [ops (gensym "ops")]
    `(do
       (defn ~name ~params ~body)
       (fn [~ops] (vector ~ops))
    )))

