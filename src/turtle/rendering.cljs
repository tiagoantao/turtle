(ns turtle.rendering
  "Support for rendering.

These are general functions to support rendering engines
"
  {:author "Tiago Antao"}
  (:require [turtle.core :as turtle]))


(defn draw-states
  "Goes through all states of a movement and draws them.

  Drawing function is given by the user"
  [draw-state canvas states start]
  (let [[state & rest-states] states]
    (draw-state canvas start state)
    (if (nil? rest-states)
      state
      (draw-states draw-state canvas rest-states state))))


(defn play-movements
  "Goes through all movements and converts them to states. Needs flat list"
  [draw-state canvas movements start]

  (let [[movement & rest-movements] movements
        states (movement start)
        last-state (last states)]
    (draw-states draw-state canvas states start)
    (if (not (nil? rest-movements))
      (play-movements draw-state canvas rest-movements last-state))))

(defn render-state
  "Writes the state to the console, for debugging"
  [canvas start end]
  (prn "start" start)
  (prn "end" end))

