(ns turtle.core-test
  (:require [cljs.test :refer-macros [deftest is testing]]
            [turtle.core :as tcore]))

(def ^:private zero {:x 0 :y 0 :direction 90})

(deftest deg2rad
  (testing "Degrees to radians"
    (is (<
         (Math/abs
          (- (tcore/deg2rad 180) 3.1415))
         0.01))))

(deftest pos
  (testing "Setting position"
    (let [[state] ((tcore/pos 100 200) {})
          {x :x y :y :draw draw} state]
      (is (= x 100))
      (is (= y 200))
      (is (not draw)))))

(deftest pop
  (testing "Stack pop"
    (let [state ((tcore/pop) {:stack [{:x 100 :y 200 :direction 90}]})
          {x :x y :y direction :direction} (first state)]
      (is (= x 100))
      (is (= y 200))
      (is (= direction 90)))))

(deftest scale
  (testing "Scale"
    (prn (tcore/scale '((turtle.core/fwd 100) turtle.core-test/zero) 2 3))
    ;; talk about namepaces in eval
    ))


